<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.24 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# pythonanywhere 0.3.7

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_pythonanywhere/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_pythonanywhere)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_pythonanywhere/release0.3.6?logo=python)](
    https://gitlab.com/aedev-group/aedev_pythonanywhere/-/tree/release0.3.6)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_pythonanywhere)](
    https://pypi.org/project/aedev-pythonanywhere/#history)

>aedev_pythonanywhere module 0.3.7.

[![Coverage](https://aedev-group.gitlab.io/aedev_pythonanywhere/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_pythonanywhere/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_pythonanywhere/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_pythonanywhere/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_pythonanywhere/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_pythonanywhere/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_pythonanywhere)](
    https://gitlab.com/aedev-group/aedev_pythonanywhere/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_pythonanywhere)](
    https://gitlab.com/aedev-group/aedev_pythonanywhere/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_pythonanywhere)](
    https://gitlab.com/aedev-group/aedev_pythonanywhere/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_pythonanywhere)](
    https://pypi.org/project/aedev-pythonanywhere/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_pythonanywhere)](
    https://gitlab.com/aedev-group/aedev_pythonanywhere/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_pythonanywhere)](
    https://libraries.io/pypi/aedev-pythonanywhere)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_pythonanywhere)](
    https://pypi.org/project/aedev-pythonanywhere/#files)


## installation


execute the following command to install the
aedev.pythonanywhere module
in the currently active virtual environment:
 
```shell script
pip install aedev-pythonanywhere
```

if you want to contribute to this portion then first fork
[the aedev_pythonanywhere repository at GitLab](
https://gitlab.com/aedev-group/aedev_pythonanywhere "aedev.pythonanywhere code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_pythonanywhere):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_pythonanywhere/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.pythonanywhere.html
"aedev_pythonanywhere documentation").
